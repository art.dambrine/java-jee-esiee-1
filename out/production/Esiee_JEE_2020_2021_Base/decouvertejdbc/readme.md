# Echange entre JAVA et un SGBD

**JDBC** : Java Data Base Connectivity

-> `java.sql.*`

Nous encontrerons certianement des  `SQLExceptions`

Note : toutes les actions autre que le select ont pour résultat un entier

Action qui écrit : retourne un entier

Action qui lit : retourne un string

## Tuto Docker mysql rapidos

```
docker run --name=tp-java-mysql-8 -e MYSQL_ROOT_PASSWORD=password123456 -d -p 3666:3306 mysql:8

sudo docker exec -it tp-java-mysql-8 bash
mysql -u root -p

# Note Password is : password123456


SET PASSWORD FOR 'root'@'localhost' = PASSWORD('adminadmin');
ALTER USER 'root'@'localhost' IDENTIFIED BY 'adminadmin';
CREATE USER 'userjava'@'%' IDENTIFIED BY 'adminadmin';
CREATE DATABASE userjava;
GRANT ALL PRIVILEGES ON userjava.* TO 'userjava'@'%';
ALTER USER 'userjava'@'%' IDENTIFIED WITH mysql_native_password BY 'adminadmin';
EXIT;
```


### Set timezone MySQL
```
SET time_zone = 'Europe/Paris';
```
