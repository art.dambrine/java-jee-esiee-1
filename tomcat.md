<h2><img  src="./img/logo.png"  alt="tomcat"  width="50"  style="float: right;"/> Installer Tomcat</h2>

### Référence

Ce tuto est un résumé de video youtube (explication de qualité pour débuter)

https://www.youtube.com/watch?v=u_InEBgRVcc



## Intro

Webserver opensource server for hotsting Java web applications.

Technologies Java supportées :

- Java servelet

- JavaServer Pages (JSP)

- Java Expression Language (EL)

- Java Websocket

On appelle aussi Tomcat -  *Servelet container*

NOTE : **Tomcat 9 est LTS (supporte Java 1.8+)**

Page de téléchargement : **https://tomcat.apache.org/download-90.cgi**

Pour les utilisateurs Windows - prendre le lien : [**32-bit/64-bit Windows Service Installer**](https://apache.belnet.be/tomcat/tomcat-8/v8.5.59/bin/apache-tomcat-8.5.59.exe)      

Documentation Tomcat 9 : http://tomcat.apache.org/tomcat-9.0-doc/index.html



## Config d'un Tomcat - service d'installation (windows)

![image-20201117164055450](./img/tomcat-image.png)

NOTE : on met les ports désirés, le plus important est le port http. Le "Server Shutdown Port" comme son nom l'indique vous permettra depuis l'extérieur un script d'éteindre le Tomcat (Set to `-1` to disable the shutdown port.).

Note de la documentation : Disabling the shutdown port works well when Tomcat is started      using [Apache Commons Daemon](http://commons.apache.org/daemon/)      (running as a service on Windows or with jsvc on un*xes). It cannot be      used when running Tomcat with the standard shell scripts though, as it      will prevent shutdown.bat|.sh and catalina.bat|.sh from stopping it gracefully.

Le chemin d'accès à Tomcat (pour windows) : `C:\Program Files\Apache Software Foundation`  



## Les repertoires et fichiers clés de Tomcat

- **/bin** : contient les scripts pour lancer et arrêter le serveur Tomcat (.bat, .sh etc.. en fonction de l'OS)
- **/conf** : fichiers de config du serveur
- **/lib** : fichiers jar qui seront utilisés par le serveur
- **/logs** : logs du server ;)
- **/temp** : fichiers temporaires utilisés par la JVM
- **/webapps** : c'est là que vous mettrez votre jolie application. (si vous avez choisit d'installer les exemples de Tomcat lors de l'installation il devrait déjà y avoir du monde ici)
- **/work** : fichiers temporaires pour nos applications deployées



## Lancer / arrêter le serveur Tomcat

Plusieurs possibilités :

- Avec les services Windows
- Lancer un startup.bat du **/bin** (nécessite que JAVA_HOME soit enregistré dans le PATH)
- Avec le `Tomcat9.exe` (simple double-clic)

NOTE : Ajouter JAVA_HOME au PATH Windows https://javatutorial.net/set-java-home-windows-10



## Déployer ses Webapps sur le serveur Tomcat



C'est bon pour les prérequis de la prochaine séance, vous pouvez toujours poursuivre le tutoriel à partir d'ici pour un complément sur comment déployer les Webapps sur votre Tomcat 

 https://youtu.be/u_InEBgRVcc?t=1199

 NOTE: Pour la suite nous utiliserons la plateforme de developpement Java EE afin de générer un fichier .war executable par notre serveur Tomcat.
 Nous passerons donc sur un autre projet (notre projet en cours étant un projet classique Java SE).

Rappel - merci stackoverflow : "Java EE is a set of specifications that are bundled together to form a platform. If you need to develop a web application, then you need to use atleast Servlets and JSP specifications which are subsets of the Java EE specification."

