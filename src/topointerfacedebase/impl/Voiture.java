package topointerfacedebase.impl;

import topointerfacedebase.api.Demarrable;

import java.awt.*;

//
// Une voiture EST qqc de Demarrable
//

public class Voiture implements Demarrable {
    private Color couleur;
    private int puissance;
    private boolean enFonctionnement;

    public Voiture(Color couleur, int puissance) {
        this.couleur = couleur;
        this.puissance = puissance;
        this.enFonctionnement = false;
    }

    public Color getCouleur() {
        return couleur;
    }

    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = puissance;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "couleur=" + couleur +
                ", puissance=" + puissance +
                ", enFonctionnement=" + enFonctionnement +
                '}';
    }


    @Override
    public void on() {
        if (!enFonctionnement) {

            System.out.println("\t La voiture démarre");

            enFonctionnement = true;
        }
    }

    @Override
    public void off() {
        if (enFonctionnement) {
            System.out.println("\t La voiture s'arrête");

            enFonctionnement = false;
        }
    }

    // cf. javadoc :
    // https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/Comparable.html#compareTo(T)

    /*@Override
    public int compareTo(Voiture o) {
        // Classons les voitures selon la composante verte
        return Integer.compare(this.couleur.getGreen(), o.getCouleur().getGreen());
    }*/
}
