package topointerfacedebase.utilisation;

import topointerfacedebase.impl.Voiture;

import java.awt.*;
import java.util.Arrays;

public class TestBase {

    // astuce : 'psvm' + tab
    public static void main(String[] args) {

        System.out.println("\n".repeat(1) + "Debut de TestBase :");
        System.out.println("\n".repeat(1) + "-".repeat(15) + "\n".repeat(1));

        /* Code here : */

        Voiture[] desVoitures = {
                new Voiture(Color.CYAN, 80),
                new Voiture(Color.BLACK, 120),
                new Voiture(Color.GRAY, 70),
                new Voiture(Color.GREEN, 90)
        };


        System.out.println("Avant le tri :");
        for (Voiture v : desVoitures) {
            System.out.println(v);
        }

        // Exemple 2 : https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/util/Comparator.html#method.detail
        // (Ici on n'a pas la main sur la classe voiture)
        ComparateurDeVoituresRouges monComparateur = new ComparateurDeVoituresRouges();
        Arrays.sort(desVoitures, monComparateur);


        System.out.println("\n");
        System.out.println("Après le tri :");
        for (Voiture v : desVoitures) {
            System.out.println(v);
        }


        /* Fin d'execution de notre programme */
        System.out.println("\n".repeat(1) + "-".repeat(15) + "\n".repeat(1));
        System.out.println("Fin de TestBase.");
    }
}
