package topointerfacedebase.utilisation;

import topointerfacedebase.impl.Voiture;

import java.util.Comparator;

public class ComparateurDeVoituresRouges implements Comparator<Voiture> {
    @Override
    public int compare(Voiture o1, Voiture o2) {

        return Integer.compare(o1.getCouleur().getRed(), o2.getCouleur().getRed());

    }
}
