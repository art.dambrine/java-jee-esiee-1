package topointerfacedebase.api;

public interface Demarrable {

    void on();

    void off();

}
