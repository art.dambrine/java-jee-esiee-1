package decouvertejdbc;

import java.sql.*;

public class TestJDBC {

    public static void main(String[] args) {
        /*
         * Première étape : trouver le nom du Driver JDBC pour votre SGBD
         * */

        String nomDuDriver = "com.mysql.cj.jdbc.Driver";

        /*
         * 2dn étape : Format des URL JDBC de votre SGBD
         * */

        String urlBD = "jdbc:mysql://127.0.0.1:3666/userjava";
        String monUser = "userjava";
        String monPassword = "adminadmin";

        /*
         * Etape n°3 : Charger le driver JDBC de votre SGBD en mémoire
         * */

        try {

            Class.forName(nomDuDriver);
            System.out.println("Le driver est chargé en mémoire \n");

        } catch (ClassNotFoundException e) {

            System.out.println("Le chargement du driver a échoué \n");
            e.printStackTrace();

            System.exit(3); // On quitte le programme (code a titre exceptionnel!!)
        }


        /*
         * Etape 4 : Elaborer une connexion entre votre programme et le SGBD
         * */

        Connection maConnection = null;

        try {
            maConnection = DriverManager.getConnection(urlBD, monUser, monPassword);
            System.out.println("La connexion au SGBD a pu être créee");
        } catch (SQLException throwables) {
            System.out.println("La connexion au SGBD a échouée");
            throwables.printStackTrace();
            System.exit(4); // A titre exceptionnel !!!
        }


        /*
         * Etape n°5 : Fabrication du Statement ("l'enveloppe")
         * */

        Statement stmt = null;

        try {
            stmt = maConnection.createStatement();
            System.out.println("Le statement a été créé avec succès.");
        } catch (SQLException throwables) {
            System.out.println("La création du statement a échouée.");
            throwables.printStackTrace();
            System.exit(5); // A titre exceptionnel !!!
        }


        /*
         * Etape n°6 : Requêtage du SGBD sans SELECT !!
         * */

        ResultSet rs;

        String requeteSqlSansSelect = "INSERT INTO test (nom) VALUES('java-test')";

        int responseInt;
        try {
            responseInt = stmt.executeUpdate(requeteSqlSansSelect);
            System.out.println("Le resultat de la requete sans select est : " + responseInt);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        /*
         * Etape n°7 : Requêtage du SGBD avec SELECT :)
         * */
        String requeteAvecSelect = "SELECT * FROM userjava.test";
        try {
            rs = stmt.executeQuery(requeteAvecSelect);

            System.out.println("\n" + "Resultat de la requête :" + "\n");

            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                if (i == 1) System.out.print("|");
                System.out.print(metaData.getColumnName(i));
                System.out.print("(" + metaData.getColumnTypeName(i) + ")|");
                if (i == columnCount) System.out.print("\n");
            }

            while (rs.next()) {
                for (int i = 1; i <= columnCount; i++) {

                    if (metaData.getColumnTypeName(i).equals("DATETIME")) {
                        // on peut donner l'index i ou le nom de la colonne "date"
                        Timestamp timestamp = rs.getTimestamp("date");
                        System.out.print(" " + timestamp);
                    } else {
                        System.out.print(" " + rs.getString(i));
                    }

                }
                System.out.print("\n");
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        /*
         * ~8ième Etape : Libération des ressources (important, ne pas oublier pour préserver le réseau, ressources mémoires etc ...)
         * */
        try {
            stmt.close();
            maConnection.close();

            System.out.println("\n".repeat(2) + "==".repeat(1) + "FIN" + "==".repeat(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
}
