# Cours JAVA JEE - n*1

Christophe Logé

email : christophe.loge@u-picardie.fr

## Mise en place de l'environnement 

Télécharger le JDK : 15.x.x

Note: Le JDK emporte tout ce qu'on a besoin pour travailler



## Création d'un projet de base (astuces et bonnes pratiques)

* Un pojo : "plain old java object"



### Raccourcis

```
'sout' + tab 
```

```
'psvm' + tab
```



### Lecture des erreurs



**Astuce** : les erreurs sont à lire de façon ascendante

![image-20201110151811367](./img/image-20201110151811367.png)

Résolution : Sorts the specified array of objects into ascending order, according to the [natural ordering](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/Comparable.html) of its elements. All elements in the array must implement the [`Comparable`](https://docs.oracle.com/en/java/javase/14/docs/api/java.base/java/lang/Comparable.html) interface.  Furthermore, all elements in the array must be *mutually comparable*.

Il faut implémenter l'interface 'comparable'



Dans ce cours nous avons vu deux cas :

- Implémenter une interface lorsqu'on a le contrôle sur la classe cible
- Implémenter une interface lorsqu'on a pas le contrôle sur la classe (par exemple déjà compilée)